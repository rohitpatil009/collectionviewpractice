//
//  SelfSizingCollectionViewCell.h
//  MaterialDesignSample
//
//  Created by Rohit Patil on 15/12/19.
//  Copyright © 2019 Patil Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SelfSizingCollectionViewCell : UICollectionViewCell

- (void)setTitle:(NSString *)title andSubtitle:(NSString *)subtitle;

- (void)setTitleText:(NSString *)title;

@end

NS_ASSUME_NONNULL_END
