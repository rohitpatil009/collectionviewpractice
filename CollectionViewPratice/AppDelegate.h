//
//  AppDelegate.h
//  CollectionViewPratice
//
//  Created by Rohit Patil on 05/01/20.
//  Copyright © 2020. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

