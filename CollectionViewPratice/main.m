//
//  main.m
//  CollectionViewPratice
//
//  Created by Rohit Patil on 05/01/20.
//  Copyright © 2020 . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
