#import "FullWidthCollectionViewLayout.h"

NS_ASSUME_NONNULL_BEGIN

@implementation FullWidthCollectionViewLayout

- (instancetype)init {
    self = [super init];
    if (self) {
        self.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize;
    }
    return self;
}

- (nullable NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray<UICollectionViewLayoutAttributes *> *layoutAttributes = [super layoutAttributesForElementsInRect:rect];
    
    for (UICollectionViewLayoutAttributes *attribute in layoutAttributes) {
        if (attribute.representedElementCategory == UICollectionElementCategoryCell) {
            attribute.frame = [self layoutAttributesForItemAtIndexPath:attribute.indexPath].frame;
        }
    }
    
    return layoutAttributes;
}

- (nullable UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewLayoutAttributes *attribute = [super layoutAttributesForItemAtIndexPath:indexPath];
    
    CGRect frame = attribute.frame;
    frame.size.width = self.collectionView.safeAreaLayoutGuide.layoutFrame.size.width;
    frame.origin.x = self.sectionInset.left;
    attribute.frame = frame;
    
    return attribute;
}

@end

NS_ASSUME_NONNULL_END
