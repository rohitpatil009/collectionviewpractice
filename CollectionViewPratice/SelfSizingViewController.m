//
//  ViewController.m
//  CollectionViewPratice
//
//  Created by Rohit Patil on 05/01/20.
//  Copyright © 2020 . All rights reserved.
//

#import "SelfSizingViewController.h"

#import "SelfSizingCollectionViewCell.h"
#import "FullWidthCollectionViewLayout.h"

@interface SelfSizingViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, readonly) UICollectionView *collectionView;

@end

@implementation SelfSizingViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        FullWidthCollectionViewLayout *collectionViewLayout = [[FullWidthCollectionViewLayout alloc] init];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:collectionViewLayout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColor.whiteColor;
    self.title = @"Self Sizing Cells!";
    
    self.collectionView.backgroundColor = UIColor.blueColor;
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.collectionView];
    [NSLayoutConstraint activateConstraints:@[
      [self.collectionView.topAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.topAnchor constant:10.0],
      [self.collectionView.bottomAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.bottomAnchor constant:-10.0],
      [self.collectionView.leadingAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.leadingAnchor constant:10.0],
      [self.collectionView.trailingAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.trailingAnchor constant:-10.0],
    ]];
    
    Class cellClass = [SelfSizingCollectionViewCell class];
    [self.collectionView registerClass:cellClass forCellWithReuseIdentifier:NSStringFromClass(cellClass)];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 5;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *reuseIdentifier = NSStringFromClass([SelfSizingCollectionViewCell class]);
    SelfSizingCollectionViewCell *cell =
    (SelfSizingCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                                              forIndexPath:indexPath];
    [self configureCell:cell forIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(SelfSizingCollectionViewCell *)cell forIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.item) {
        case 0:
            [cell setTitle:@"This is small title." andSubtitle:@"This is small subtitle."];
            break;
            
        case 1:
            [cell setTitle:@"This is little big title" andSubtitle:@"This is little big subtitle."];
            break;
            
        case 2:
            [cell setTitle:@"This is must be really really really really really really really really really really really really really really really big title."
               andSubtitle:@"This is little big subtitle."];
            break;
            
        case 3:
            [cell setTitle:@"This is must be really really really really really really really really really really really really really really really big title.big title.big title.big title."
               andSubtitle:@"This is subtitle."];
            break;
            
        case 4:
            [cell setTitle:@"This is must be really really really really really really really really really really really really really really really big title."
               andSubtitle:@"This is must be really really really really really really really really really really really really really really really big subtitle."];
            break;
            
        default: {
            static NSUInteger count = 1;
            NSString *subtitle = [NSString stringWithFormat:@"- For Default case %lu", count];
            [cell setTitle:@"Some title" andSubtitle:subtitle];
            count = count == 200 ? 0 : count + 1;
            
            break;
        }
    }
}

@end
