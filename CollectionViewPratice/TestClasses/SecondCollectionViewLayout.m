//
//  SecondCollectionViewLayout.m
//  CollectionViewPratice
//
//  Created by Rohit Patil on 12/01/20.
//  Copyright © 2020 . All rights reserved.
//

#import "SecondCollectionViewLayout.h"

static const NSUInteger kNumberOfColumns = 2;
static const NSUInteger kCellPadding = 6;

@interface SecondCollectionViewLayout ()

@property (nonatomic, readonly) NSMutableArray<UICollectionViewLayoutAttributes *> *cache;

@property (nonatomic) CGFloat contentHeight;
@property (nonatomic) CGFloat contentWidth;

@end

@implementation SecondCollectionViewLayout

- (instancetype)init {
    self = [super init];
    if (self) {
        _cache = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)prepareLayout {
    if (self.cache.count > 0  || self.collectionView == nil) {
        // Only calculate the layout attributes if cache is empty and the collection view exists.
        return;
    }

    CGFloat xOffset = self.collectionView.contentInset.left;
    CGFloat yOffset = 0;
    CGFloat itemWidth = self.collectionView.bounds.size.width;

    for (NSUInteger item = 0; item < [self.collectionView numberOfItemsInSection:0]; item++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:0];
        CGFloat itemHeight = [self.delegate collectionView:self.collectionView
                                                    layout:self
                                    heightForItemWithWidth:itemWidth
                                               atIndexPath:indexPath];

        CGRect frame = CGRectMake(xOffset, yOffset, itemWidth, itemHeight);
        CGRect insetFrame = CGRectInset(frame, 0, kCellPadding);
        
        UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
        attributes.frame = insetFrame;
        [self.cache addObject:attributes];
        
        self.contentHeight = MAX(self.contentHeight, CGRectGetMaxX(insetFrame));
        yOffset += itemHeight;
    }
}

- (nullable NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {
    [super layoutAttributesForElementsInRect:rect];
    NSMutableArray<UICollectionViewLayoutAttributes *> *visibleAttributes = [NSMutableArray array];
    
    for (UICollectionViewLayoutAttributes *attribute in self.cache) {
        if (CGRectIntersectsRect(attribute.frame, rect)) {
            [visibleAttributes addObject:attribute];
        }
    }
    
    return visibleAttributes;
}

- (CGSize)collectionViewContentSize {
    return CGSizeMake(self.contentWidth, self.contentHeight);
}

- (nullable UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    return self.cache[indexPath.item];
}

@end
