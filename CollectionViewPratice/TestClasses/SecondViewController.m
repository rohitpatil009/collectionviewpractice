//
//  ViewController.m
//  CollectionViewPratice
//
//  Created by Rohit Patil on 05/01/20.
//  Copyright © 2020 . All rights reserved.
//

#import "SecondViewController.h"

#import "SelfSizingCollectionViewCell.h"
#import "SecondCollectionViewLayout.h"

NS_ASSUME_NONNULL_BEGIN

@interface SecondViewController () <UICollectionViewDelegate, UICollectionViewDataSource, SecondCollectionViewLayoutDelegate>

@property (nonatomic, readonly) UICollectionView *collectionView;

@end

@implementation SecondViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        SecondCollectionViewLayout *collectionViewLayout = [[SecondCollectionViewLayout alloc] init];
        collectionViewLayout.delegate = self;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:collectionViewLayout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColor.whiteColor;
    self.title = @"Self Sizing Cells!";
    
    self.collectionView.backgroundColor = UIColor.blueColor;
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.collectionView];
    [NSLayoutConstraint activateConstraints:@[
      [self.collectionView.topAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.topAnchor constant:10.0],
      [self.collectionView.bottomAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.bottomAnchor constant:-10.0],
      [self.collectionView.leadingAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.leadingAnchor constant:10.0],
      [self.collectionView.trailingAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.trailingAnchor constant:-10.0],
    ]];
    
    Class cellClass = [SelfSizingCollectionViewCell class];
    [self.collectionView registerClass:cellClass forCellWithReuseIdentifier:NSStringFromClass(cellClass)];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 5;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *reuseIdentifier = NSStringFromClass([SelfSizingCollectionViewCell class]);
    SelfSizingCollectionViewCell *cell =
    (SelfSizingCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                                              forIndexPath:indexPath];
    [self configureCell:cell forIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(SelfSizingCollectionViewCell *)cell forIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.item) {
        case 0:
            [cell setTitle:@"This is small title." andSubtitle:@"This is small subtitle."];
            break;
            
        case 1:
            [cell setTitle:@"This is little big title" andSubtitle:@"This is little big subtitle."];
            break;
            
        case 2:
            [cell setTitle:@"This is must be really really really really really really really really really really really really really really really big title."
               andSubtitle:@"This is little big subtitle."];
            break;
            
        case 3:
            [cell setTitle:@"This is must be really really really really really really really really really really really really really really really big title.big title.big title.big title."
               andSubtitle:@"This is subtitle."];
            break;
            
        case 4:
            [cell setTitle:@"This is must be really really really really really really really really really really really really really really really big title."
               andSubtitle:@"This is must be really really really really really really really really really really really really really really really big subtitle."];
            break;
            
        default: {
            static NSUInteger count = 1;
            NSString *subtitle = [NSString stringWithFormat:@"- For Default case %lu", count];
            [cell setTitle:@"Some title" andSubtitle:subtitle];
            count = count == 200 ? 0 : count + 1;
            
            break;
        }
    }
}

- (CGFloat)collectionView:(nonnull UICollectionView *)collectionView
                   layout:(nonnull SecondCollectionViewLayout *)layout
   heightForItemWithWidth:(CGFloat)width
              atIndexPath:(nonnull NSIndexPath *)indexPath {
    static SelfSizingCollectionViewCell *sizingCell;
    if (sizingCell == nil) {
        sizingCell = [[SelfSizingCollectionViewCell alloc] init];
    }
    [self configureCell:sizingCell forIndexPath:indexPath];
//    CGSize fittingSize = [sizingCell systemLayoutSizeFittingSize:CGSizeMake(width, 0.0)
//                                   withHorizontalFittingPriority:UILayoutPriorityRequired
//                                         verticalFittingPriority:UILayoutPriorityFittingSizeLevel];
    [sizingCell layoutIfNeeded];
    CGSize fittingSize = [sizingCell systemLayoutSizeFittingSize:CGSizeMake(width, 0.0)
                                   withHorizontalFittingPriority:UILayoutPriorityRequired
                                         verticalFittingPriority:UILayoutPriorityFittingSizeLevel];

    NSLog(@" $$## fitting height: %f", fittingSize.height);
    
    CGFloat heightFromCell = [sizingCell heightForWidth];
    NSLog(@" $$## heightFromCell: %f", heightFromCell);
    return fittingSize.height;
}

@end

NS_ASSUME_NONNULL_END
