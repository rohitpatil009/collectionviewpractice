//
//  SecondCollectionViewLayout.h
//  CollectionViewPratice
//
//  Created by Rohit Patil on 12/01/20.
//  Copyright © 2020 . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SecondCollectionViewLayoutDelegate;
NS_ASSUME_NONNULL_BEGIN

@interface SecondCollectionViewLayout : UICollectionViewLayout

@property(nonatomic, weak) id<SecondCollectionViewLayoutDelegate> delegate;

@end

@protocol SecondCollectionViewLayoutDelegate <NSObject>

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(SecondCollectionViewLayout *)layout
   heightForItemWithWidth:(CGFloat)width
              atIndexPath:(NSIndexPath *)indexPath;

@end

NS_ASSUME_NONNULL_END
