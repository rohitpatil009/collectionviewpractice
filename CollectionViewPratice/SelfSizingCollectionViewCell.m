//
//  SelfSizingCollectionViewCell.m
//  MaterialDesignSample
//
//  Created by Rohit Patil on 15/12/19.
//  Copyright © 2019 Patil Corp. All rights reserved.
//

#import "SelfSizingCollectionViewCell.h"

@interface SelfSizingCollectionViewCell ()

@property (nonatomic, readonly) UILabel *title;
@property (nonatomic, readonly) UILabel *subtitle;

@end

@implementation SelfSizingCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _title = [[UILabel alloc] init];
        _title.numberOfLines = 0;
        _title.backgroundColor = UIColor.yellowColor;
        _title.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_title];
        
        _subtitle = [[UILabel alloc] init];
        _subtitle.numberOfLines = 0;
        _subtitle.backgroundColor = UIColor.greenColor;
        _subtitle.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_subtitle];
        
        [NSLayoutConstraint activateConstraints:@[
            [_title.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:5.0],
            [_title.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor constant:5.0],
            [_title.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor constant:-5.0],
                                                  
            [_subtitle.topAnchor constraintEqualToAnchor:_title.bottomAnchor constant:5.0],
            [_subtitle.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor constant:5.0],
            [_subtitle.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor constant:-5.0],
            [_subtitle.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor constant:-5.0],
        ]];
        
        self.contentView.backgroundColor = UIColor.cyanColor;
    }
    return self;
}

- (void)setTitle:(NSString *)title andSubtitle:(NSString *)subtitle {
    self.title.text = title;
    self.subtitle.text = subtitle;
}

- (void)setTitleText:(NSString *)title {
    self.title.text = title;
    self.subtitle.hidden = YES;
}

- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    UICollectionViewLayoutAttributes *attribute = [super preferredLayoutAttributesFittingAttributes:layoutAttributes];

    [self layoutIfNeeded];
    CGRect frame = attribute.frame;
    frame.size = [self systemLayoutSizeFittingSize:UILayoutFittingCompressedSize
                      withHorizontalFittingPriority:UILayoutPriorityRequired
                            verticalFittingPriority:UILayoutPriorityFittingSizeLevel];
    attribute.frame = frame;
    return attribute;
}

- (void)prepareForReuse {
    self.title.text = nil;
    self.subtitle.text = nil;
    self.subtitle.hidden = NO;
}

@end
