#import <UIKit/UIKit.h>

@protocol FullWidthCollectionViewLayoutDelegate;

NS_ASSUME_NONNULL_BEGIN

@interface FullWidthCollectionViewLayout : UICollectionViewFlowLayout

@end

NS_ASSUME_NONNULL_END
